package mansioMMO;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.BorderFactory;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;


public class VentanaLogin extends JFrame implements ActionListener 
{
	JPanel pnPanel0;
	JLabel lbLabel0;
	JLabel lbLabel1;
	JTextField txtUsuario;
	JTextField txtContra;
	JButton btnLogin;
	JLabel lbLabel2;


	public VentanaLogin() 
	{
		super( "Iniciar sesión" );

	   pnPanel0 = new JPanel();
	   GridBagLayout gbPanel0 = new GridBagLayout();
	   GridBagConstraints gbcPanel0 = new GridBagConstraints();
	   pnPanel0.setLayout( gbPanel0 );
	
	   lbLabel0 = new JLabel( "Usuario"  );
	   gbcPanel0.gridx = 3;
	   gbcPanel0.gridy = 4;
	   gbcPanel0.gridwidth = 4;
	   gbcPanel0.gridheight = 1;
	   gbcPanel0.fill = GridBagConstraints.BOTH;
	   gbcPanel0.weightx = 1;
	   gbcPanel0.weighty = 1;
	   gbcPanel0.anchor = GridBagConstraints.NORTH;
	   gbPanel0.setConstraints( lbLabel0, gbcPanel0 );
	   pnPanel0.add( lbLabel0 );
	
	   lbLabel1 = new JLabel( "Contra"  );
	   gbcPanel0.gridx = 3;
	   gbcPanel0.gridy = 7;
	   gbcPanel0.gridwidth = 4;
	   gbcPanel0.gridheight = 1;
	   gbcPanel0.fill = GridBagConstraints.BOTH;
	   gbcPanel0.weightx = 1;
	   gbcPanel0.weighty = 1;
	   gbcPanel0.anchor = GridBagConstraints.NORTH;
	   gbPanel0.setConstraints( lbLabel1, gbcPanel0 );
	   pnPanel0.add( lbLabel1 );
	
	   txtUsuario = new JTextField( );
	   gbcPanel0.gridx = 9;
	   gbcPanel0.gridy = 4;
	   gbcPanel0.gridwidth = 6;
	   gbcPanel0.gridheight = 1;
	   gbcPanel0.fill = GridBagConstraints.BOTH;
	   gbcPanel0.weightx = 1;
	   gbcPanel0.weighty = 0;
	   gbcPanel0.anchor = GridBagConstraints.NORTH;
	   gbPanel0.setConstraints( txtUsuario, gbcPanel0 );
	   pnPanel0.add( txtUsuario );
	
	   txtContra = new JPasswordField( );
	   gbcPanel0.gridx = 9;
	   gbcPanel0.gridy = 7;
	   gbcPanel0.gridwidth = 6;
	   gbcPanel0.gridheight = 1;
	   gbcPanel0.fill = GridBagConstraints.BOTH;
	   gbcPanel0.weightx = 1;
	   gbcPanel0.weighty = 0;
	   gbcPanel0.anchor = GridBagConstraints.NORTH;
	   gbPanel0.setConstraints( txtContra, gbcPanel0 );
	   pnPanel0.add( txtContra );
	
	   btnLogin = new JButton( "Login"  );
	   gbcPanel0.gridx = 5;
	   gbcPanel0.gridy = 9;
	   gbcPanel0.gridwidth = 8;
	   gbcPanel0.gridheight = 2;
	   gbcPanel0.fill = GridBagConstraints.BOTH;
	   gbcPanel0.weightx = 1;
	   gbcPanel0.weighty = 0;
	   gbcPanel0.anchor = GridBagConstraints.NORTH;
	   gbPanel0.setConstraints( btnLogin, gbcPanel0 );
	   pnPanel0.add( btnLogin );
	   btnLogin.addActionListener(this);
	
	   lbLabel2 = new JLabel( ""  );
	   gbcPanel0.gridx = 3;
	   gbcPanel0.gridy = 1;
	   gbcPanel0.gridwidth = 12;
	   gbcPanel0.gridheight = 1;
	   gbcPanel0.fill = GridBagConstraints.BOTH;
	   gbcPanel0.weightx = 1;
	   gbcPanel0.weighty = 1;
	   gbcPanel0.anchor = GridBagConstraints.NORTH;
	   gbPanel0.setConstraints( lbLabel2, gbcPanel0 );
	   pnPanel0.add( lbLabel2 );
	
	   setDefaultCloseOperation( EXIT_ON_CLOSE );
	
	   setContentPane( pnPanel0 );
	   pack();
	   setSize(360,150);
	   setVisible( true );
	} 
	
	public void actionPerformed(ActionEvent ae) {
		
		if(ae.getSource() == btnLogin) {
			String respuesta = "";
			String tokenSesion = "";
			String nombreJugador = "";
			
			
			String inputUsuario = txtUsuario.getText();
			String Contra = txtContra.getText();
			int idJugador = -1;
			
			respuesta = Principal.MMOSYNC.iniciarSesion(inputUsuario, Contra);
			String[] parts = respuesta.split(",");
			
			nombreJugador = inputUsuario;
			idJugador = Integer.parseInt(parts[0]);
			tokenSesion = parts[1];
			
			if(idJugador > 0) {
				Principal.idJugador = idJugador;
				Principal.tokenSesion = tokenSesion;
				this.dispose();
				Principal.EmpezarJuego();
				
			} else {
				lbLabel2.setText("Usuario/Contraseña incorrecto.");
			}
		} 
	}
} 
