package mansioMMO;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class Jugador {
	private int idJugadorDB;
	
	private int sexeJugador;
	private int camiseta;
	private int pantalones;
	private int zapatos;
	private int cara;
	private int peinado;
	private int habitacionID=0;
	private int tamano_inventario = 20;
	private int pos_inventario = 0;
	Objetos inventario[];
	
	public Jugador(int idJugador) {
		this.pos_inventario = 0;
		this.pantalones = 1;
		this.cara = 1;
		this.zapatos = 1;
		this.idJugadorDB = idJugador;
		this.inventario = new Objetos[tamano_inventario];
		CargarPersonaje();
		CargarInventario();
	}
	
	
	public void CargarInventario() {
		String json_personaje = Principal.MMOSYNC.CargarInventario(Principal.idJugador, Principal.tokenSesion);
		
		Object obj=JSONValue.parse(json_personaje);
		
		JSONArray array=(JSONArray)obj;
		for(int i = 0; i< array.size(); i++) {
			int IDArray = i;
			JSONObject Inventario=(JSONObject)array.get(IDArray);
			int idObjeto =Integer.parseInt((String)Inventario.get("objeto_id"));
			DarObjeto(Principal.DBObjetos.obtenerObj(idObjeto));
		}
	}
	
	public void GuardarInventario() {
		String json_inventario = "";
		for (int x=0; x < this.tamano_inventario-1; x++) {
			if(this.inventario[x] != null) {
				json_inventario += "{\"objeto_id\": \""+this.inventario[x].getObjetoID()+"\"},";
			}
		}
		
		if (json_inventario.endsWith(",")) {
			json_inventario = json_inventario.substring(0, json_inventario.length() - 1);
		}
		
		Principal.MMOSYNC.GuardarInventario(Principal.idJugador,Principal.tokenSesion,"["+json_inventario+"]");	
	}
	
	public void GuardarPersonaje() {
		String json_personaje = "{ \"personaje\": {\"sx\": \""+this.getSexo()+"\",\"camiseta\": \""+this.getCamiseta()+"\",\"pantalon\": \""+this.getPantalones()+"\",\"pelo\": \""+this.getPeinado()+"\",\"perro_contento\": \""+Principal.PerroFuera+"\" ,\"habitacion\": \""+this.getHabitacionID()+"\"}}";
		Principal.MMOSYNC.GuardarPersonaje(Principal.idJugador,Principal.tokenSesion,json_personaje);	
	}
	
	public void CargarPersonaje() {
		String json_personaje = Principal.MMOSYNC.CargarPersonaje(Principal.idJugador, Principal.tokenSesion);
		Object obj=JSONValue.parse(json_personaje);
		
		JSONArray array=(JSONArray)obj;
		for(int i = 0; i< array.size(); i++) {
			int IDArray = i;
			JSONObject Personaje=(JSONObject)array.get(IDArray);
			setHabitacionID(Integer.parseInt((String)Personaje.get("Habitacion")));
			setSexe(Integer.parseInt((String)Personaje.get("Sexo")));
			setCamiseta(Integer.parseInt((String)Personaje.get("Camiseta")));
			setPeinado(Integer.parseInt((String)Personaje.get("Pelo")));
			
			int estadoPerro = Integer.parseInt((String)Personaje.get("perro"));
			
			if(estadoPerro== 1) {
				Principal.PerroFuera = true;
			}
		}
	}
	
	public void DarObjeto(Objetos objeto) {
		if(this.pos_inventario < 20) {
			this.inventario[this.pos_inventario] = objeto;
			pos_inventario++;
		} else {
			for (int x=0; x < this.tamano_inventario-1; x++) {
				if(this.inventario[x] == null) {
					this.inventario[x] = objeto;
				}
			}
		}
		
	}
	
	public void RetirarObjeto(Objetos objeto) {
		for (int x=0; x < this.tamano_inventario-1; x++) {
			if(this.inventario[x] == objeto) {
				this.inventario[x] = null;
			}
		}
	}
	
	public boolean TieneObjeto(Objetos objeto) {
		for (int x=0; x < this.tamano_inventario-1; x++) {
			if(this.inventario[x] == objeto) {
				return true;
			}
		}
		return false;
	}
	
	
	/*--SETERS---*/
	public void setSexe(int sexe) {
		this.sexeJugador=sexe;
	}
	
	public void setCamiseta(int camiseta) {
		this.camiseta=camiseta;
	}
	
	public void setPantalones(int pantalones) {
		this.pantalones = pantalones;
	}
	
	public void setZapatos(int zapatos) {
		this.zapatos = zapatos;
	}
	
	public void setCara(int cara) {
		this.cara = cara;
	}
	
	public void setPeinado(int peinado) {
		this.peinado = peinado;
	}
	
	public void setHabitacionID(int habitacion) {
		this.habitacionID = habitacion;
	}
	
	/*---GETERS----*/
	public int getSexo() { return this.sexeJugador; }
	public int getCamiseta() { return this.camiseta; }
	public int getPantalones() { return this.pantalones; }
	public int getZapatos() { return this.zapatos; }
	public int getCara() { return this.cara; }
	public int getPeinado() { return this.peinado; }
	public int getHabitacionID() { return this.habitacionID; }
	
	public Objetos[] obtenerInventario() { return this.inventario; }
	public int obtenerEspacioUsado() { return this.pos_inventario; }
}
