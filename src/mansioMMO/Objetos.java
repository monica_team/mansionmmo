package mansioMMO;

public abstract class Objetos {
	int idObjeto;
	String nombreObjeto;
	String descripcionObjeto;
	int idTipoObjeto;
	
	public String getNombreObjeto() { return this.nombreObjeto; }
	public int getTipoObjeto() { return this.idTipoObjeto; }
	public int getObjetoID() { return this.idObjeto; }
	public String getDescripcionObjeto() { return this.descripcionObjeto; }
	
	
}
