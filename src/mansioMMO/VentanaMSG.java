package mansioMMO;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JPanel;
import javax.swing.BorderFactory;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JButton;

class VentanaMSG extends JFrame  {
	JPanel pnPanel0;
	JLabel labelAdvertencia;

	VentanaMSG() 
	{
	   super( "Notificacion" );

	   pnPanel0 = new JPanel();
	   GridBagLayout gbPanel0 = new GridBagLayout();
	   GridBagConstraints gbcPanel0 = new GridBagConstraints();
	   pnPanel0.setLayout( gbPanel0 );

	   

	   labelAdvertencia = new JLabel( "Mensaje"  );
	   gbcPanel0.gridx = 4;
	   gbcPanel0.gridy = 1;
	   gbcPanel0.gridwidth = 13;
	   gbcPanel0.gridheight = 1;
	   gbcPanel0.fill = GridBagConstraints.BOTH;
	   gbcPanel0.weightx = 1;
	   gbcPanel0.weighty = 1;
	   gbcPanel0.anchor = GridBagConstraints.NORTH;
	   gbPanel0.setConstraints( labelAdvertencia, gbcPanel0 );
	   pnPanel0.add( labelAdvertencia );

	   setContentPane( pnPanel0 );
	   pack();
	   setVisible( true );
	   setSize(600,200);
	}

	
	
	public void MostrarAdvertencia(String advertencia) {
		labelAdvertencia.setText(advertencia);
	}

}
