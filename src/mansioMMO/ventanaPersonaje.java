package mansioMMO;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JRadioButton;

public class ventanaPersonaje extends JFrame implements ActionListener 
{
	private int idSexo = 1;
	private int idPelo = 1;
	private int idCamiseta = 1;
	private int idPantalones = 1;
	
	private int maxPeloHombre = 4;
	private int maxCamisetaHombre = 10;
	
	private int maxPeloMujer = 6;
	private int maxCamisetaMujer = 23;
	
	String url_hombre = "http://cancolapimmo.apps.tecnomakers.net/imagenes/Hombre/hombre_";
	String url_mujer = "http://cancolapimmo.apps.tecnomakers.net/imagenes/Mujer/mujer_";
	
	JPanel pnPanel0;
	ButtonGroup rbgPanel0;
	JLabel imagenPersonaje;
	JButton sigPelo;
	JButton sigCamiseta;
	JButton antCamiseta;
	JButton btnGuardar;
	JButton antPelo;
	JLabel lbLabel2;
	JLabel lbLabel3;
	JRadioButton radioButtonHombre;
	JRadioButton radioButtonMujer;

	ventanaPersonaje() throws MalformedURLException 
	{
	   super( "Selector de avatar" );
	
	   pnPanel0 = new JPanel();
	   rbgPanel0 = new ButtonGroup();
	   GridBagLayout gbPanel0 = new GridBagLayout();
	   GridBagConstraints gbcPanel0 = new GridBagConstraints();
	   pnPanel0.setLayout( gbPanel0 );
	
	   imagenPersonaje = new JLabel("");
	   gbcPanel0.gridx = 6;
	   gbcPanel0.gridy = 4;
	   gbcPanel0.gridwidth = 8;
	   gbcPanel0.gridheight = 15;
	   gbcPanel0.fill = GridBagConstraints.BOTH;
	   gbcPanel0.weightx = 1;
	   gbcPanel0.weighty = 1;
	   gbcPanel0.anchor = GridBagConstraints.NORTH;
	   gbPanel0.setConstraints( imagenPersonaje, gbcPanel0 );
	   pnPanel0.add( imagenPersonaje );
	
	   sigPelo = new JButton( ">>"  );
	   gbcPanel0.gridx = 15;
	   gbcPanel0.gridy = 4;
	   gbcPanel0.gridwidth = 3;
	   gbcPanel0.gridheight = 2;
	   gbcPanel0.fill = GridBagConstraints.BOTH;
	   gbcPanel0.weightx = 1;
	   gbcPanel0.weighty = 1;
	   gbcPanel0.anchor = GridBagConstraints.NORTH;
	   gbPanel0.setConstraints( sigPelo, gbcPanel0 );
	   pnPanel0.add( sigPelo );
	   sigPelo.addActionListener(this);
	
	   sigCamiseta = new JButton( ">>"  );
	   gbcPanel0.gridx = 15;
	   gbcPanel0.gridy = 12;
	   gbcPanel0.gridwidth = 3;
	   gbcPanel0.gridheight = 2;
	   gbcPanel0.fill = GridBagConstraints.BOTH;
	   gbcPanel0.weightx = 1;
	   gbcPanel0.weighty = 1;
	   gbcPanel0.anchor = GridBagConstraints.NORTH;
	   gbPanel0.setConstraints( sigCamiseta, gbcPanel0 );
	   pnPanel0.add( sigCamiseta );
	   sigCamiseta.addActionListener(this);
	
	   antCamiseta = new JButton( "<<"  );
	   gbcPanel0.gridx = 2;
	   gbcPanel0.gridy = 12;
	   gbcPanel0.gridwidth = 3;
	   gbcPanel0.gridheight = 2;
	   gbcPanel0.fill = GridBagConstraints.BOTH;
	   gbcPanel0.weightx = 1;
	   gbcPanel0.weighty = 1;
	   gbcPanel0.anchor = GridBagConstraints.NORTH;
	   gbPanel0.setConstraints( antCamiseta, gbcPanel0 );
	   pnPanel0.add( antCamiseta );
	   antCamiseta.addActionListener(this);
	
	   antPelo = new JButton( "<<"  );
	   gbcPanel0.gridx = 2;
	   gbcPanel0.gridy = 4;
	   gbcPanel0.gridwidth = 3;
	   gbcPanel0.gridheight = 2;
	   gbcPanel0.fill = GridBagConstraints.BOTH;
	   gbcPanel0.weightx = 1;
	   gbcPanel0.weighty = 1;
	   gbcPanel0.anchor = GridBagConstraints.NORTH;
	   gbPanel0.setConstraints( antPelo, gbcPanel0 );
	   pnPanel0.add( antPelo );
	   antPelo.addActionListener(this);
	
	   lbLabel2 = new JLabel(new ImageIcon(new URL("http://cancolapimmo.apps.tecnomakers.net/imagenes/hombre.png"))  );
	   gbcPanel0.gridx = 7;
	   gbcPanel0.gridy = 2;
	   gbcPanel0.gridwidth = 1;
	   gbcPanel0.gridheight = 1;
	   gbcPanel0.fill = GridBagConstraints.BOTH;
	   gbcPanel0.weightx = 1;
	   gbcPanel0.weighty = 1;
	   gbcPanel0.anchor = GridBagConstraints.NORTH;
	   gbPanel0.setConstraints( lbLabel2, gbcPanel0 );
	   pnPanel0.add( lbLabel2 );
	
	   lbLabel3 = new JLabel(new ImageIcon(new URL("http://cancolapimmo.apps.tecnomakers.net/imagenes/hembra.png")) );
	   gbcPanel0.gridx = 13;
	   gbcPanel0.gridy = 2;
	   gbcPanel0.gridwidth = 1;
	   gbcPanel0.gridheight = 1;
	   gbcPanel0.fill = GridBagConstraints.BOTH;
	   gbcPanel0.weightx = 1;
	   gbcPanel0.weighty = 1;
	   gbcPanel0.anchor = GridBagConstraints.NORTH;
	   gbPanel0.setConstraints( lbLabel3, gbcPanel0 );
	   pnPanel0.add( lbLabel3 );
	
	   radioButtonHombre = new JRadioButton( ""  );
	   radioButtonHombre.setSelected( false );
	   rbgPanel0.add( radioButtonHombre );
	   gbcPanel0.gridx = 6;
	   gbcPanel0.gridy = 2;
	   gbcPanel0.gridwidth = 1;
	   gbcPanel0.gridheight = 1;
	   gbcPanel0.fill = GridBagConstraints.BOTH;
	   gbcPanel0.weightx = 1;
	   gbcPanel0.weighty = 0;
	   gbcPanel0.anchor = GridBagConstraints.NORTH;
	   gbPanel0.setConstraints( radioButtonHombre, gbcPanel0 );
	   pnPanel0.add( radioButtonHombre );
	   radioButtonHombre.addActionListener(this);
	
	   radioButtonMujer = new JRadioButton( ""  );
	   radioButtonMujer.setSelected( false );
	   rbgPanel0.add( radioButtonMujer );
	   gbcPanel0.gridx = 12;
	   gbcPanel0.gridy = 2;
	   gbcPanel0.gridwidth = 1;
	   gbcPanel0.gridheight = 1;
	   gbcPanel0.fill = GridBagConstraints.BOTH;
	   gbcPanel0.weightx = 1;
	   gbcPanel0.weighty = 0;
	   gbcPanel0.anchor = GridBagConstraints.NORTH;
	   gbPanel0.setConstraints( radioButtonMujer, gbcPanel0 );
	   pnPanel0.add( radioButtonMujer );
	   radioButtonMujer.addActionListener(this);
	   
	   
	   btnGuardar = new JButton( "Guardar"  );
	   gbcPanel0.gridx = 1;
	   gbcPanel0.gridy = 20;
	   gbcPanel0.gridwidth = 20;
	   gbcPanel0.gridheight = 1;
	   gbcPanel0.fill = GridBagConstraints.BOTH;
	   gbcPanel0.weightx = 1;
	   gbcPanel0.weighty = 1;
	   gbcPanel0.anchor = GridBagConstraints.NORTH;
	   gbPanel0.setConstraints( btnGuardar, gbcPanel0 );
	   pnPanel0.add( btnGuardar );
	   btnGuardar.addActionListener(this);
	
	   setContentPane( pnPanel0 );
	   pack();
	   setVisible( true );
	   setSize(360,600);
	   ObtenerAvatarActual();
	   ActualizarImagenAvatar();
	   
	} 
	
	public String devolverUrlImagen() {
		String url_final = "";
		if(this.idSexo == 1) {
			//Hombre
			url_final = this.url_hombre+"pe"+this.idPelo+"_c"+this.idCamiseta+"_pa"+this.idPantalones+".jpg";
		} else if(this.idSexo == 2) {
			//Mujer
			url_final = this.url_mujer+"pe"+this.idPelo+"_c"+this.idCamiseta+"_pa"+this.idPantalones+".jpg";
		}
		
		return url_final;
	}
	
	public void actionPerformed(ActionEvent ae) {
		
		if(ae.getSource() == sigPelo)
		{
			if(this.idSexo == 1) {
				if(this.idPelo < this.maxPeloHombre) {
					this.idPelo ++;
				}
			} else if(this.idSexo == 2) {
				if(this.idPelo < this.maxPeloMujer) {
					this.idPelo ++;
				}
			}
		} 
		else if(ae.getSource() == antPelo) {
			if(this.idPelo > 1) {
				this.idPelo --;
			}
		}
		
		
		else if(ae.getSource() == sigCamiseta) {
			if(this.idSexo == 1) {
				if(this.idCamiseta < this.maxCamisetaHombre) {
					this.idCamiseta  ++;
				}
			} else if(this.idSexo == 2) {
				if(this.idCamiseta < this.maxCamisetaMujer) {
					this.idCamiseta  ++;
				}
			}
		}
		else if(ae.getSource() == antCamiseta) {
			if(this.idCamiseta > 1) {
				this.idCamiseta --;
			}
		}
		else if(ae.getSource() == radioButtonHombre) {
			this.idSexo = 1;
			idPelo = 1;
			idCamiseta = 1;
		}
		else if(ae.getSource() == radioButtonMujer) {
			this.idSexo = 2;
			idPelo = 1;
			idCamiseta = 1;
		}
		else if(ae.getSource() == btnGuardar) {
			Principal.J1.GuardarPersonaje();
			Principal.J1.setCamiseta(this.idCamiseta);
			Principal.J1.setPeinado(this.idPelo);
			Principal.J1.setSexe(this.idSexo);
		}
		
		ActualizarImagenAvatar();
	}
	
	public void cambiarImagenAvatar() throws MalformedURLException {
		ImageIcon icon = new ImageIcon(new URL(devolverUrlImagen()));
		/*
		Image img = icon.getImage();

		BufferedImage bi = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

		Graphics g = bi.createGraphics();
		g.drawImage(img, 0, 0, 178, 470, null);*/
		
		
		Image img = icon.getImage();
		Image newimg = img.getScaledInstance(178, 470,  java.awt.Image.SCALE_SMOOTH);
		
		ImageIcon imageFinal = new ImageIcon(newimg);
		imageFinal.getImage().flush();
		imagenPersonaje.setIcon( imageFinal );
	}
	
	public void ActualizarImagenAvatar() {
		try {
			cambiarImagenAvatar();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}
	
	public void ObtenerAvatarActual() {
		this.idSexo = Principal.J1.getSexo();
		this.idCamiseta = Principal.J1.getCamiseta();
		this.idPelo = Principal.J1.getPeinado();
		
		if(this.idSexo == 1) {
			radioButtonHombre.setSelected( true );
		} else if(this.idSexo == 2) {
			radioButtonMujer.setSelected( true );
		}
	}
} 