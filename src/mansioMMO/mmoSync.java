package mansioMMO;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;

public class mmoSync {
	final String URL_Servidor = "http://cancolapimmo.apps.tecnomakers.net";
	
	public String obtenerURL() {
		return this.URL_Servidor;
	}
	
	public String iniciarSesion(String Usuario, String password) {
		String LineaRespuesta="";
		try {
			String json = "{ \"login\": {\"nombre_usuario\": \""+Usuario+"\",\"contra\": \""+getMD5(password)+"\"}}";			
			String URL_FINAL = this.URL_Servidor+"/api.php?accion=iniciar_sesion&json="+json;
			
			BufferedReader rd = peticionJSON(URL_FINAL);
			String line;
			while ((line = rd.readLine()) != null) {
				LineaRespuesta = LineaRespuesta+line;
			}
			rd.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return LineaRespuesta;
	}
	
	public void GuardarPersonaje(int idUsuario,String token,String json) {
		try {			
			String URL_FINAL = this.URL_Servidor+"/api.php?accion=core&modulo=guardar_personaje&id_usuario="+idUsuario+"&tokenSesion="+token+"&json="+json;
			String LineaRespuesta="";
			//System.out.println(URL_FINAL);
			BufferedReader rd = peticionJSON(URL_FINAL);String line;
			while ((line = rd.readLine()) != null) {
				LineaRespuesta = LineaRespuesta+line;
			}
			//System.out.println(URL_FINAL);
			rd.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String CargarPersonaje(int idUsuario,String token) {
		String jsonPersonaje="";
		try {			
			String URL_FINAL = this.URL_Servidor+"/api.php?accion=core&modulo=cargar_personaje&id_usuario="+idUsuario+"&tokenSesion="+token;
			String LineaRespuesta="";
			//System.out.println(URL_FINAL);
			BufferedReader rd = peticionJSON(URL_FINAL);
			String line;
			while ((line = rd.readLine()) != null) {
				jsonPersonaje = jsonPersonaje+line;
			}
			rd.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonPersonaje;
	}
	
	public void GuardarInventario(int idUsuario,String token,String json) {
		try {			
			String URL_FINAL = this.URL_Servidor+"/api.php?accion=core&modulo=guardar_inventario&id_usuario="+idUsuario+"&tokenSesion="+token+"&json="+json;
			String LineaRespuesta="";

			BufferedReader rd = peticionJSON(URL_FINAL);String line;
			while ((line = rd.readLine()) != null) {
				LineaRespuesta = LineaRespuesta+line;
			}
			//System.out.println(URL_FINAL);
			rd.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String CargarInventario(int idUsuario,String token) {
		String jsonPersonaje="";
		try {			
			String URL_FINAL = this.URL_Servidor+"/api.php?accion=core&modulo=cargar_inventario&id_usuario="+idUsuario+"&tokenSesion="+token;
			String LineaRespuesta="";
			BufferedReader rd = peticionJSON(URL_FINAL);
			String line;
			while ((line = rd.readLine()) != null) {
				jsonPersonaje = jsonPersonaje+line;
			}
			rd.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonPersonaje;
	}
	
	public BufferedReader peticionJSON(String URL) {
		BufferedReader rd = null;
		try {
			URL url = new URL(URL);
			URLConnection conn = url.openConnection();
			// Get the response
			rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return rd;
	}
	
	public static String getMD5(String input) {
        byte[] source;
        try {
            //Get byte according by specified coding.
            source = input.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            source = input.getBytes();
        }
        String result = null;
        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7',
                '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(source);
            //The result should be one 128 integer
            byte temp[] = md.digest();
            char str[] = new char[16 * 2];
            int k = 0;
            for (int i = 0; i < 16; i++) {
                byte byte0 = temp[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            result = new String(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
	
	public void MantenerSesion(final int idUsuario,final String token) {
		try {			
			String URL_FINAL = this.URL_Servidor+"/api.php?accion=core&modulo=mantener_conexion&id_usuario="+idUsuario+"&tokenSesion="+token;
			BufferedReader rd = peticionJSON(URL_FINAL);
			rd.close();
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	public void IniciarTimerSesion(final int idUsuario,final String token) {
		new Thread() {
			@Override
			public void run() {
				boolean mantener=true;
				do {
					try {
						MantenerSesion(idUsuario,token);
						Principal.J1.GuardarPersonaje();
						Thread.sleep(10000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} while(mantener != false);
				
			}
		}.start();
	}

}
