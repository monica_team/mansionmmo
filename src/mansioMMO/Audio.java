package mansioMMO;

import java.io.*;
import javazoom.jl.player.Player;

public class Audio {
	
	public static void playClip() {
		//para que se pueda repetir siempre
		final boolean loopMusica = true;
		
		new Thread() {
			  @Override
			  public void run() {
				  int musicaAnterior = -1;
				  int musicaActual = -1;
				  int maxMusica = 8;
				  
				 //Repetimos la musica para tus oidos
				  do
			      {
			    	  //Musica de ambientacion para la mansion.
					  String strAl = "";
					  Random al = new Random();
					  
					  musicaAnterior = musicaActual;
					  musicaActual = al.GenerarAleatorio(1, maxMusica);
					  if(musicaActual == musicaAnterior) {
						  //Algo mas de variedad porfavor
						  do {
							  musicaActual = al.GenerarAleatorio(1,maxMusica);
						  } while(musicaActual == musicaAnterior);
					  }
					  
					  switch(musicaActual) {
						  case 1:
							  strAl = "docks.mp3";
							  break;
						  case 2:
							  strAl = "main.mp3";
							  break;
						  case 3:
							  strAl = "mutecity.mp3";
							  break;
						  case 4:
							  strAl = "forest.mp3";
							  break;
						  case 5:
							  strAl = "epicenter.mp3";
							  break;
						  case 6:
							  strAl = "gadd-garage.mp3";
							  break;
						  case 7:
							  strAl = "western.mp3";
							  break;
						  case 8:
							  strAl = "koopa.mp3";
							  break;
					  }
					  
					  //Leer el archivo y utilizando la libreria jl1.0
					  try(FileInputStream fis = new FileInputStream("musicas/"+strAl)){
					    	  new Player(fis).play();
					   }catch(Exception e){System.out.println(e);}
			      }while(loopMusica); 
			  }
		}.start();
		
	}
	
	public static void sonidoAlerta() {
		//Mostrar un sonido de alerta al hacer algo mal
		
		new Thread() {

			  @Override
			  public void run() {
				  String strAl = "alerta.mp3";				  
				  //Leer el archivo y utilizando la libreria jl1.0
				  try(FileInputStream fis = new FileInputStream(strAl)){
					  new Player(fis).play();
				        
				  }catch(Exception e){System.out.println(e);}
			       
			  }
			}.start();
		
	}
}
