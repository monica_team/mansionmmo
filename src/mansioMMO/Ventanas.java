package mansioMMO;

import java.awt.*;
import javax.swing.*;
import java.net.*;
import java.io.*;
import java.awt.event.*;


class Ventanas extends JFrame implements ActionListener
{
    //Atributos
	JPanel pnPanel0;
	JButton btnIzquierda;
	JButton btnNorte;
	JButton btnSur;
	JButton btnDerecha;
	JButton btnArriba;
	JButton btnAbajo;
	
	JPanel pnPanel1;
	JLabel lbLabel0;
	
	JList listadoInventario;
	JList listadoHabitacion;
	
	DefaultListModel listModelHabitacion = null;
	DefaultListModel listModelInventario = null;
	
	int totalObjetosHabitacion;
	int totalObjetosPersonaje;
	Objetos[] inventarioHabitacion;
	Objetos[] inventarioPersonaje;
       
	Ventanas()
    {
		super( "MansioMMO" );
	
		
		
		 pnPanel0 = new JPanel();
		   GridBagLayout gbPanel0 = new GridBagLayout();
		   GridBagConstraints gbcPanel0 = new GridBagConstraints();
		   pnPanel0.setLayout( gbPanel0 );

		   pnPanel1 = new JPanel();
		   GridBagLayout gbPanel1 = new GridBagLayout();
		   GridBagConstraints gbcPanel1 = new GridBagConstraints();
		   pnPanel1.setLayout( gbPanel1 );

		   lbLabel0 = new JLabel(new ImageIcon("cuartos/Cuarto_0.jpg")  );
		   gbcPanel1.gridx = 4;
		   gbcPanel1.gridy = 0;
		   gbcPanel1.gridwidth = 12;
		   gbcPanel1.gridheight = 16;
		   gbcPanel1.fill = GridBagConstraints.BOTH;
		   gbcPanel1.weightx = 1;
		   gbcPanel1.weighty = 1;
		   gbcPanel1.anchor = GridBagConstraints.NORTH;
		   gbPanel1.setConstraints( lbLabel0, gbcPanel1 );
		   pnPanel1.add( lbLabel0 );

		   listModelInventario = new DefaultListModel();
		   listadoInventario = new JList( listModelInventario );
		   gbcPanel1.gridx = 0;
		   gbcPanel1.gridy = 0;
		   gbcPanel1.gridwidth = 4;
		   gbcPanel1.gridheight = 16;
		   gbcPanel1.fill = GridBagConstraints.BOTH;
		   gbcPanel1.weightx = 1;
		   gbcPanel1.weighty = 1;
		   gbcPanel1.anchor = GridBagConstraints.NORTH;
		   gbPanel1.setConstraints( listadoInventario, gbcPanel1 );
		   pnPanel1.add( listadoInventario );
		   listadoInventario.addMouseListener(listenerInventario);

		   listModelHabitacion = new DefaultListModel();
		  
		   listadoHabitacion = new JList( listModelHabitacion );
		   gbcPanel1.gridx = 17;
		   gbcPanel1.gridy = 0;
		   gbcPanel1.gridwidth = 5;
		   gbcPanel1.gridheight = 16;
		   gbcPanel1.fill = GridBagConstraints.BOTH;
		   gbcPanel1.weightx = 1;
		   gbcPanel1.weighty = 1;
		   gbcPanel1.anchor = GridBagConstraints.NORTH;
		   gbPanel1.setConstraints( listadoHabitacion, gbcPanel1 );
		   pnPanel1.add( listadoHabitacion );
		   gbcPanel0.gridx = 0;
		   gbcPanel0.gridy = 0;
		   gbcPanel0.gridwidth = 20;
		   gbcPanel0.gridheight = 16;
		   gbcPanel0.fill = GridBagConstraints.BOTH;
		   gbcPanel0.weightx = 1;
		   gbcPanel0.weighty = 0;
		   gbcPanel0.anchor = GridBagConstraints.NORTH;
		   gbPanel0.setConstraints( pnPanel1, gbcPanel0 );
		   pnPanel0.add( pnPanel1 );
		   listadoHabitacion.addMouseListener(listenerHabitacion);

		   btnIzquierda = new JButton( "Izquierda"  );
		   gbcPanel0.gridx = 0;
		   gbcPanel0.gridy = 17;
		   gbcPanel0.gridwidth = 3;
		   gbcPanel0.gridheight = 2;
		   gbcPanel0.fill = GridBagConstraints.BOTH;
		   gbcPanel0.weightx = 1;
		   gbcPanel0.weighty = 0;
		   gbcPanel0.anchor = GridBagConstraints.NORTH;
		   gbPanel0.setConstraints( btnIzquierda, gbcPanel0 );
		   pnPanel0.add( btnIzquierda );
		   btnIzquierda.addActionListener(this);

		   btnNorte = new JButton( "Norte"  );
		   gbcPanel0.gridx = 4;
		   gbcPanel0.gridy = 17;
		   gbcPanel0.gridwidth = 3;
		   gbcPanel0.gridheight = 2;
		   gbcPanel0.fill = GridBagConstraints.BOTH;
		   gbcPanel0.weightx = 1;
		   gbcPanel0.weighty = 0;
		   gbcPanel0.anchor = GridBagConstraints.NORTH;
		   gbPanel0.setConstraints( btnNorte, gbcPanel0 );
		   pnPanel0.add( btnNorte );
		   btnNorte.addActionListener(this);

		   btnSur = new JButton( "Sur"  );
		   gbcPanel0.gridx = 8;
		   gbcPanel0.gridy = 17;
		   gbcPanel0.gridwidth = 3;
		   gbcPanel0.gridheight = 2;
		   gbcPanel0.fill = GridBagConstraints.BOTH;
		   gbcPanel0.weightx = 1;
		   gbcPanel0.weighty = 0;
		   gbcPanel0.anchor = GridBagConstraints.NORTH;
		   gbPanel0.setConstraints( btnSur, gbcPanel0 );
		   pnPanel0.add( btnSur );
		   btnSur.addActionListener(this);

		   btnDerecha = new JButton( "Derecha"  );
		   gbcPanel0.gridx = 12;
		   gbcPanel0.gridy = 17;
		   gbcPanel0.gridwidth = 3;
		   gbcPanel0.gridheight = 2;
		   gbcPanel0.fill = GridBagConstraints.BOTH;
		   gbcPanel0.weightx = 1;
		   gbcPanel0.weighty = 0;
		   gbcPanel0.anchor = GridBagConstraints.NORTH;
		   gbPanel0.setConstraints( btnDerecha, gbcPanel0 );
		   pnPanel0.add( btnDerecha );
		   btnDerecha.addActionListener(this);

		   btnArriba = new JButton( "Arriba"  );
		   gbcPanel0.gridx = 16;
		   gbcPanel0.gridy = 16;
		   gbcPanel0.gridwidth = 3;
		   gbcPanel0.gridheight = 2;
		   gbcPanel0.fill = GridBagConstraints.BOTH;
		   gbcPanel0.weightx = 1;
		   gbcPanel0.weighty = 0;
		   gbcPanel0.anchor = GridBagConstraints.NORTH;
		   gbPanel0.setConstraints( btnArriba, gbcPanel0 );
		   pnPanel0.add( btnArriba );
		   btnArriba.addActionListener(this);
		   
		   btnAbajo = new JButton(  "Abajo");
		   gbcPanel0.gridx = 16;
		   gbcPanel0.gridy = 18;
		   gbcPanel0.gridwidth = 3;
		   gbcPanel0.gridheight = 2;
		   gbcPanel0.fill = GridBagConstraints.BOTH;
		   gbcPanel0.weightx = 1;
		   gbcPanel0.weighty = 0;
		   gbcPanel0.anchor = GridBagConstraints.NORTH;
		   gbPanel0.setConstraints( btnAbajo, gbcPanel0 );
		   pnPanel0.add( btnAbajo );
		   btnAbajo.addActionListener(this);

	   setDefaultCloseOperation( EXIT_ON_CLOSE );
	   setContentPane( pnPanel0 );
	   pack();
	   setVisible( true );
	   setSize(1100,600);
        
    }

	public void actionPerformed(ActionEvent ae) {
		int idHabitacion = Principal.J1.getHabitacionID();
		int nuevaHabitacion = -1;
		int objetoRequerido = -1;
		
		if(ae.getSource() == btnArriba)
		{
			nuevaHabitacion = Principal.mansion.devolverHabitacion(idHabitacion).getEscaleraArriba();
			objetoRequerido = Principal.mansion.devolverHabitacion(idHabitacion).getObjEscaleraArriba();
		} 
		else if(ae.getSource() == btnAbajo) {
			nuevaHabitacion = Principal.mansion.devolverHabitacion(idHabitacion).getEscaleraAbajo();
			objetoRequerido = Principal.mansion.devolverHabitacion(idHabitacion).getObjEscaleraAbajo();
		}
		else if(ae.getSource() == btnIzquierda) {
			nuevaHabitacion = Principal.mansion.devolverHabitacion(idHabitacion).getDireccionIzquierda();
			objetoRequerido = Principal.mansion.devolverHabitacion(idHabitacion).getObjIzquierda();
		}
		else if(ae.getSource() == btnDerecha) {
			nuevaHabitacion = Principal.mansion.devolverHabitacion(idHabitacion).getDireccionDerecha();
			objetoRequerido = Principal.mansion.devolverHabitacion(idHabitacion).getObjDerecha();
		}
		else if(ae.getSource() == btnNorte) {
			nuevaHabitacion = Principal.mansion.devolverHabitacion(idHabitacion).getDireccionArriba();
			objetoRequerido = Principal.mansion.devolverHabitacion(idHabitacion).getObjArriba();
		}
		else if(ae.getSource() == btnSur) {
			nuevaHabitacion = Principal.mansion.devolverHabitacion(idHabitacion).getDireccionAbajo();
			objetoRequerido = Principal.mansion.devolverHabitacion(idHabitacion).getObjAbajo();
		}
	
		
		if(nuevaHabitacion != -1) {
			if(objetoRequerido != -1) {
				if(Principal.J1.TieneObjeto(Principal.DBObjetos.obtenerObj(objetoRequerido))) {
					cambiarHabitacion(nuevaHabitacion);
				} else {
					Principal.MostrarMensajeAdvertencia("No tienes el objeto que se necesita: "+Principal.DBObjetos.obtenerObj(objetoRequerido).getNombreObjeto());
				}
			} else {
				cambiarHabitacion(nuevaHabitacion);
			}
			
		}
	}

	public void cambiarHabitacion(int idhabitacion) {
		String nuevaImagen = "";
		nuevaImagen = Principal.mansion.devolverHabitacion(idhabitacion).getImagen();
		cambiarImagenCuarto(nuevaImagen);
		cambiarListadoHabitacion(idhabitacion);
		Principal.J1.setHabitacionID(idhabitacion);
	}
	
	public void cambiarImagenCuarto(String imagenCuarto) {
		imagenCuarto = "cuartos/"+imagenCuarto;
		ImageIcon icon = new ImageIcon(imagenCuarto);
		icon.getImage().flush();
		lbLabel0.setIcon( icon );
	}
	
	public void cambiarListadoHabitacion(int idhabitacion) {
		
		int totalObjetos = Principal.mansion.devolverHabitacion(idhabitacion).getCantidadObjetos();
		String NombreObjeto = "";
		listModelHabitacion.clear();
		listModelHabitacion.addElement("--Objetos habitacion--");
		Objetos [] inventario = Principal.mansion.devolverHabitacion(idhabitacion).listadoObjetos();
		inventarioHabitacion = inventario;
		totalObjetosHabitacion = 0;
		for (int x=0; x < totalObjetos; x++) {
			totalObjetosHabitacion +=1;
			NombreObjeto = inventario[x].getNombreObjeto();
			listModelHabitacion.addElement(NombreObjeto);
		}
	}

	public void cambiarObjetosInventario() {
		String NombreObjeto = "";
		listModelInventario.clear();
		listModelInventario.addElement("--Inventario--");
		int totalObjetos = Principal.J1.obtenerEspacioUsado();
		Objetos[] inventario = new Objetos[20];
		inventario = Principal.J1.obtenerInventario();
		
		inventarioPersonaje = inventario;
		totalObjetosPersonaje = totalObjetos;
		for (int x=0; x < totalObjetos; x++) {
			if(inventario[x] != null) {
				NombreObjeto = inventario[x].getNombreObjeto();
				listModelInventario.addElement(NombreObjeto);
			}
		}
	}
	
	MouseListener listenerInventario = new MouseAdapter() {
		public void mouseClicked(MouseEvent mouseEvent) {
	        if (mouseEvent.getClickCount() == 2) {
	        	int index = listadoInventario.locationToIndex(mouseEvent.getPoint());
	        	if(index == 0) {
	        		Principal.MostrarVentanaPersonaje();
	        	}
	        	else if (index > 0) {
	        	//	int idObjeto = inventar
	        		
	        	}
	        }
	    }
	};
	
	MouseListener listenerHabitacion = new MouseAdapter() {
		public void mouseClicked(MouseEvent mouseEvent) {
	        if (mouseEvent.getClickCount() == 2) {
	        	int index = listadoHabitacion.locationToIndex(mouseEvent.getPoint());
	        	if (index > 0) {
	        		//
	        		for (int x=0; x < totalObjetosHabitacion; x++) {
	        			Object o = listadoHabitacion.getModel().getElementAt(index);
	        			//Compruebo que el objeto concuerda por nombre
	        			if(inventarioHabitacion[x].getNombreObjeto() == o.toString()) {
	        				int idObjeto = inventarioHabitacion[x].getObjetoID();
	        				VentanaObjetos vo = new VentanaObjetos();
	        				vo.setObjetoVisualizar(idObjeto);
	        				//System.out.println("Objeto: "+inventarioHabitacion[x].getObjetoID());
	        			}
	        		
	        		}
	        	}
	        }
	    }
	};

    
    
} 
