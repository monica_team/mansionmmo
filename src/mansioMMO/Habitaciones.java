package mansioMMO;

public class Habitaciones {
	private int habitacionID;
	private int habitacion_DIR_Arriba;
	private int habitacion_DIR_Abajo;
	private int habitacion_DIR_Derecha;
	private int habitacion_DIR_Izquierda;
	private int habitacion_Obj_Arriba;
	private int habitacion_Obj_Abajo;
	private int habitacion_Obj_Derecha;
	private int habitacion_Obj_Izquierda;
	private int habitacion_escalera_Arriba;
	private int habitacion_escalera_Abajo;
	private String habitacion_imagen;
	private int habitacion_escalera_Arriba_Obj;
	private int habitacion_escalera_Abajo_Obj;
	private Objetos listadoObjetos[];
	private int cantidadObjetos;
	
	public Habitaciones(int ID, int dirArriba,int dirAbajo, int dirDerecha, int dirIzquierda, String imagen) {
		listadoObjetos = new Objetos[10];
		cantidadObjetos=0;
		this.habitacionID = ID;
		this.habitacion_DIR_Arriba = dirArriba;
		this.habitacion_DIR_Abajo = dirAbajo;
		this.habitacion_DIR_Derecha = dirDerecha;
		this.habitacion_DIR_Izquierda = dirIzquierda;
		this.habitacion_Obj_Arriba = -1;
		this.habitacion_Obj_Abajo = -1;
		this.habitacion_Obj_Derecha = -1;
		this.habitacion_Obj_Izquierda = -1;
		this.habitacion_imagen = imagen;
		this.habitacion_escalera_Arriba = -1;
		this.habitacion_escalera_Abajo = -1;
		this.habitacion_escalera_Arriba_Obj = -1;
		this.habitacion_escalera_Abajo_Obj = -1;
	}
	
	public void nuevoObjeto(Objetos tipoObjeto) {
		listadoObjetos[cantidadObjetos] = tipoObjeto;
		cantidadObjetos++;
	}
	
	public Objetos[] listadoObjetos() {
		return listadoObjetos;
	}
	
	public void quitarObjeto(int idObjeto) {
		
	}
	
	public boolean existeObjeto(Objetos tipoObjeto) {
		return false;
	}
	
	public void setHabitacionID(int id) { this.habitacionID = id; }
	public void setDireccionArriba(int dir) { this.habitacion_DIR_Arriba = dir; }
	public void setDireccionAbajo(int dir) { this.habitacion_DIR_Abajo = dir; }
	public void setDireccionIzquierda(int dir) { this.habitacion_DIR_Izquierda = dir; }
	public void setDireccionDerecha(int dir) { this.habitacion_DIR_Derecha = dir; }
	public void setObjArriba(int obj) { this.habitacion_Obj_Arriba = obj; }
	public void setObjAbajo(int obj) { this.habitacion_Obj_Abajo = obj; }
	public void setObjIzquierda(int obj) { this.habitacion_Obj_Izquierda = obj; }
	public void setObjDerecha(int obj) { this.habitacion_Obj_Derecha = obj; }
	public void setImagen(String img) { this.habitacion_imagen = img; }
	public void setObjEscaleraArriba(int obj) { this.habitacion_escalera_Arriba_Obj = obj; }
	public void setObjEscaleraAbajo(int obj) { this.habitacion_escalera_Abajo_Obj = obj; }
	
	public void setEscaleraArriba(int dir) { this.habitacion_escalera_Arriba = dir; }
	public void setEscaleraAbajo(int dir) { this.habitacion_escalera_Abajo = dir; }
	
	public int getCantidadObjetos() { return this.cantidadObjetos; }
	public String getInformacionObjeto(int idObjeto) {
		String descripcion = "";

		descripcion= listadoObjetos[idObjeto].getNombreObjeto();

		return descripcion;
	}
	
	public int gettHabitacionID() { return this.habitacionID; }
	public int getDireccionArriba() { return this.habitacion_DIR_Arriba; }
	public int getDireccionAbajo() { return this.habitacion_DIR_Abajo; }
	public int getDireccionIzquierda() { return this.habitacion_DIR_Izquierda; }
	public int getDireccionDerecha() { return this.habitacion_DIR_Derecha; }
	public int getObjArriba() { return this.habitacion_Obj_Arriba; }
	public int getObjAbajo() { return this.habitacion_Obj_Abajo; }
	public int getObjIzquierda() { return this.habitacion_Obj_Izquierda; }
	public int getObjDerecha() { return this.habitacion_Obj_Derecha; }
	public String getImagen() { return this.habitacion_imagen; }
	public int getEscaleraArriba() { return this.habitacion_escalera_Arriba; }
	public int getEscaleraAbajo() { return this.habitacion_escalera_Abajo; }
	public int getObjEscaleraArriba() { return this.habitacion_escalera_Arriba_Obj; }
	public int getObjEscaleraAbajo() { return this.habitacion_escalera_Abajo_Obj; }
}
