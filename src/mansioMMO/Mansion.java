package mansioMMO;

public class Mansion {
	int totalHabitaciones=0;
	private Habitaciones habitaciones[];
	
	public Mansion() {
		habitaciones = new Habitaciones[24];
	}
	
	public void NuevaHabitacion(int ID, int dirArriba,int dirAbajo, int dirIzquierda,int dirDerecha, String imagen) {
		habitaciones[ID] = new Habitaciones(ID, dirArriba, dirAbajo, dirDerecha, dirIzquierda, imagen);
		totalHabitaciones++;
	}
	
	public int ComprobarObjeto(int ObjetoID, int habitacionID, int lado) {
		int idObjetoHabitacion = -1;
		switch(lado) {
			case 1://Arriba
				idObjetoHabitacion = habitaciones[habitacionID].getObjArriba();
				break;
			case 2://Abajo
				idObjetoHabitacion = habitaciones[habitacionID].getObjAbajo();
				break;
			case 3://Izquierda
				idObjetoHabitacion = habitaciones[habitacionID].getObjIzquierda();
				break;
			case 4://Derecha
				idObjetoHabitacion = habitaciones[habitacionID].getObjDerecha();
				break;
		}
		return idObjetoHabitacion;
	}
	
	public void setSalida(int lado, int habitacionID, int lugarID) {
		switch(lado) {
			case 1://Arriba
				habitaciones[habitacionID].setDireccionArriba(lugarID);
				break;
			case 2://Abajo
				habitaciones[habitacionID].setDireccionAbajo(lugarID);
				break;
			case 3://Izquierda
				habitaciones[habitacionID].setDireccionIzquierda(lugarID);
				break;
			case 4://Derecha
				habitaciones[habitacionID].setDireccionDerecha(lugarID);
				break;
			case 5://Escalera arriba
				habitaciones[habitacionID].setEscaleraArriba(lugarID);
				break;
			case 6://Escalera abajo
				habitaciones[habitacionID].setEscaleraAbajo(lugarID);
				break;
		}
	}
	
	public void setObjetoSalida(int ObjetoID, int habitacionID, int lado) {
		switch(lado) {
			case 1://Arriba
				habitaciones[habitacionID].setObjArriba(ObjetoID);
				break;
			case 2://Abajo
				habitaciones[habitacionID].setObjAbajo(ObjetoID);
				break;
			case 3://Izquierda
				habitaciones[habitacionID].setObjIzquierda(ObjetoID);
				break;
			case 4://Derecha
				habitaciones[habitacionID].setObjDerecha(ObjetoID);
				break;
		}
	}
	
	public void InsertarObjetoHabitacion(int ID,Objetos tipoObjeto) {
		if(habitaciones[ID].gettHabitacionID() >= 0) {
			habitaciones[ID].nuevoObjeto(tipoObjeto);
		}
		
	}
	
	public void MoverHabitacion(int nuevaHabitacion) {
		Principal.J1.setHabitacionID(nuevaHabitacion);
	}
	
	public Habitaciones devolverHabitacion(int idHabitacion) {
		return habitaciones[idHabitacion];
	}
	
	
}
