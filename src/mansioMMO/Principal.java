package mansioMMO;

import java.net.MalformedURLException;

public class Principal {
	static Audio audio;
	static Jugador J1;
	static Ventanas v;
	static ventanaPersonaje vistaPersonaje;
	static Mansion mansion;
	static ObjetosDB DBObjetos;
	static VentanaLogin vistaLogin;
	static int idJugador;
	static String tokenSesion;
	static mmoSync MMOSYNC;
	
	static boolean PerroFuera = false;
	
	public static void main(String[] args) {
		idJugador = -1;
		PerroFuera = false;
		
		
		Audio.playClip();
		DBObjetos = new ObjetosDB();
		
		//Valores que se cargan de la base de datos
		int sexeJugador = 1;//Hombre
		int camiseta = 1;
		int pantalones = 1;
		int zapatos = 1;
		int cara=1;
		int tonoPiel = 1;
		int habitacion=0;
		
		//Generar mansion
		GenerarMansion();
		
		
		
		MMOSYNC = new mmoSync();
		
		//Nos aseguramos que el usuario inicia sesion
		MostrarLogin();
		
	
		
		
		
		
	}
	
	static public void MostrarVentanaPersonaje() {
		try {
			vistaPersonaje = new ventanaPersonaje();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}
	
	static public void MostrarLogin() {
		vistaLogin = new VentanaLogin();
	}
	
	
	static public void GenerarMansion() {
		mansion = new Mansion();
		//ID, Arriba, Abajo, Izquierda y Derecha
		mansion.NuevaHabitacion(0, -1, 3, -1, 1, "Cuarto_0.jpg");
		//Habitacion 0: Donde se inicia
		mansion.InsertarObjetoHabitacion(0,DBObjetos.obtenerObj(0));
		mansion.InsertarObjetoHabitacion(0,DBObjetos.obtenerObj(7));
		mansion.InsertarObjetoHabitacion(0,DBObjetos.obtenerObj(8));
		
		//Planta baja
		mansion.NuevaHabitacion(1, -1, 4, 0, 2, "Cuarto_1.jpg");
		mansion.InsertarObjetoHabitacion(1,DBObjetos.obtenerObj(16));
		mansion.NuevaHabitacion(2, -1, -1, 1, -1, "Cuarto_2.jpg");
		mansion.InsertarObjetoHabitacion(2,DBObjetos.obtenerObj(18));
		
		mansion.NuevaHabitacion(3, 0, 6, -1, 4, "Cuarto_3.jpg");
		mansion.NuevaHabitacion(4, 1, -1, 3, 5, "Cuarto_4.jpg");
		mansion.NuevaHabitacion(5, -1, 8, 4, -1, "Cuarto_5.jpg");
		mansion.NuevaHabitacion(6, 3, -1, -1, -1, "Cuarto_6.jpg");
		
		//Objetos necesarios para finalizar
		
		
		mansion.NuevaHabitacion(7, -1, 10, -1, -1, "Cuarto_7.jpg");
		mansion.NuevaHabitacion(8, 5, 11, -1, -1, "Cuarto_8.jpg");
		mansion.NuevaHabitacion(9, -1, -1, -1, 10, "Cuarto_9.jpg");
		mansion.NuevaHabitacion(10, 7, 23, 9, 11, "Cuarto_10.jpg");
		
		mansion.setObjetoSalida(3, 10, 2);//Llave para salir - captain obvious al rescate
		mansion.setObjetoSalida(1, 1, 4);//Llave cocina para la cocina (gallina al canto)
		mansion.setObjetoSalida(2, 10, 1);//Llave sotano para el sotano
		
		mansion.NuevaHabitacion(11, 8, -1, 10, -1, "Cuarto_11.jpg");
		
		//Planta 1
		mansion.NuevaHabitacion(17, 14, -1, 16, 18, "Cuarto_17.jpg");
		mansion.NuevaHabitacion(16, 13, -1, -1, 17, "Cuarto_16.jpg");
		mansion.NuevaHabitacion(18, 15, -1, 17, -1, "Cuarto_18.jpg");
		mansion.NuevaHabitacion(13, -1, 16, -1, -1, "Cuarto_13.jpg");
		
		
		
		
		mansion.NuevaHabitacion(14, 12, 17, -1, -1, "Cuarto_14.jpg");
		mansion.NuevaHabitacion(15, -1, 18, -1, -1, "Cuarto_15.jpg");
		
		mansion.NuevaHabitacion(12, -1, 14, -1, -1, "Cuarto_12.jpg");
		
		//Planta -1
		mansion.NuevaHabitacion(21, 19, -1, 20, 22, "Cuarto_21.jpg");
		mansion.NuevaHabitacion(19, -1, 21, -1, -1, "Cuarto_19.jpg");
		
		mansion.NuevaHabitacion(20, -1, -1, -1,21, "Cuarto_20.jpg");
		mansion.NuevaHabitacion(22, -1, -1, 21, -1, "Cuarto_22.jpg");
		
		mansion.NuevaHabitacion(23, 10, -1, -1, -1, "Cuarto_23.jpg");
		
		Random al = new Random();
		
		int HabitacionFinal;
		HabitacionFinal = al.GenerarAleatorio(1, 22);
		mansion.InsertarObjetoHabitacion(HabitacionFinal,DBObjetos.obtenerObj(5));
		mansion.InsertarObjetoHabitacion(HabitacionFinal,DBObjetos.obtenerObj(3));
		
		
		mansion.InsertarObjetoHabitacion(al.GenerarAleatorio(12, 18),DBObjetos.obtenerObj(1));//Llave de la cocina
		mansion.InsertarObjetoHabitacion(al.GenerarAleatorio(4, 11),DBObjetos.obtenerObj(2));//Llave del sotano
		
		mansion.InsertarObjetoHabitacion(al.GenerarAleatorio(19, 22),DBObjetos.obtenerObj(4));//Escalera
		
		//Escaleras
		//----Escalera arriba------
		mansion.setSalida(5,10,17);
		mansion.setSalida(5,21,7);
		
		//----Escalera Abajo-------
		mansion.setSalida(6,17,10);
		mansion.setSalida(6,7,21);
		
	}
	
	static public void EmpezarJuego() {
		J1 = new Jugador(idJugador);
		J1.CargarPersonaje();
		MMOSYNC.IniciarTimerSesion(idJugador,tokenSesion);
		
		//Interfaz grafica
		v = new Ventanas();
		v.cambiarHabitacion(J1.getHabitacionID());
		v.cambiarObjetosInventario();
		
		//Al cerrar
		Runtime.getRuntime().addShutdownHook(new Thread()
		{
		    @Override
		    public void run()
		    {
		    	//Guardar la informacion al cerrar
		    	J1.GuardarPersonaje();
		    	J1.GuardarInventario();
		    }
		});
	}
	
	static public void MostrarMensajeAdvertencia(String Mensaje) {
		VentanaMSG vmsg = new VentanaMSG();
		vmsg.MostrarAdvertencia(Mensaje);
	}
	

}
