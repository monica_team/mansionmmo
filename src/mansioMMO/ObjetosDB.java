package mansioMMO;

public class ObjetosDB {
	private Objetos obj[];
	
	public ObjetosDB() {
		obj = new Objetos[30];
		obj[0] = new Cuadro(0,"Cuadro roto", "Un cuadro demasiado roto, no sirve de nada");
		obj[1] = new Llave(1,"Llave cocina", "Una llave oxidada, abre la cocina");
		obj[2] = new Llave(2,"Llave sotano", "Una llave oxidada, abre el sotano");
		obj[3] = new Llave(3,"Llave portal", "Una llave oxidada, abre la salida a la calle");
		obj[4] = new Escalera(4,"Escalera normal", "Una escalera normal");
		obj[5] = new Perro(5,"Bulldog", "Cuidado que muerde");
		obj[6] = new Carne(6,"Carne cordero", "Un trozo de carne de cordero");
		obj[7] = new Veneno(7,"Mataratas", "Veneno mataratas");
		obj[8] = new Televisor(8,"TV vieja", "Esta estropeado, y repararla no serviria ya que le falta muchas cosas");
		obj[9] = new Hueso(9,"Hueso gallo", "Un hueso de un gallo");
		obj[10] = new Linterna(10,"Linterna vacia","Linterna sin pilas, mira tu que bien");
		obj[11] = new Veneno(11,"Aerosol proteccion","Para protegerse de los peligros");
		obj[12] = new Hueso(12,"Hueso gallina","Un hueso de una gallina");
		obj[13] = new Linterna(13,"Linterna estropeada", "Linterna con pilas, pero estropeada");
		obj[14] = new CarneEnvenenada(14,"Carne envenenada", "Carne envenada, no te la comas si no quieres ir al hospital");
		obj[15] = new Linterna(15,"Linterna", "Linterna en perfecto estado");
		obj[16] = new Cuchillo(16,"Cuchillo", "Cuchillo oxidado.");
		obj[17] = new Cuchillo(17,"Abrebotellas", "Abrebotellas. Para beber birra");
		obj[18] = new Gallina(18,"Gallina", "Ki ki ri ki");
		obj[19] = new Contenedor(19,"Nevera", "Una nevera llena de cosas");
		obj[20] = new Contenedor(20,"Armario", "Armario");
		obj[21] = new Contenedor(21,"Armario de madera", "Armario de madera");
		obj[22] = new Contenedor(22,"Guardarropas", "Guardarropas");
	}
	
	public Objetos obtenerObj(int idObjeto) {
		return obj[idObjeto];
	}
	
	public int craftear(int obj1, int obj2) {
		int objetoFinal = -1;
		if(obj1 == 6 && obj2 == 7 || obj1 == 7 && obj2 == 6 || obj1 == 6 && obj2 == 11 || obj1 == 11 && obj2 == 6) {
			objetoFinal = 14;
		}
		if(obj1 == 10 && obj2 == 13 || obj1 == 13 && obj2 == 10) {
			objetoFinal = 15;
		}
		if(obj1 == 18 && obj2 == 16 || obj1 == 16 && obj2 == 18) {
			objetoFinal = 12;
		}
		return objetoFinal;
	}
	
	public int obtenerContenedor(int idContenedor) {
		int idObjeto = -1;
		if(idContenedor == 20) {
			idObjeto = 6;
		}
		else if(idContenedor == 21) {
			idObjeto = 18;
		}
		else if(idContenedor == 22) {
			idObjeto = 16;
		}
		
		return idObjeto;
	}
}
