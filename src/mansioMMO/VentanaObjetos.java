package mansioMMO;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JPanel;
import javax.swing.BorderFactory;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JButton;

class VentanaObjetos extends JFrame implements ActionListener {
	JPanel pnPanel0;
	JLabel labelNombreObjeto;
	JButton btnCoger;
	JButton btnInteractuar;
	JLabel labelAdvertencia;
	JLabel labelDescripcion;
	
	private int idObjetoVisualizar;

	VentanaObjetos() 
	{
	   super( "Información objeto" );

	   pnPanel0 = new JPanel();
	   GridBagLayout gbPanel0 = new GridBagLayout();
	   GridBagConstraints gbcPanel0 = new GridBagConstraints();
	   pnPanel0.setLayout( gbPanel0 );

	   labelNombreObjeto = new JLabel( "NombreObjeto"  );
	   gbcPanel0.gridx = 4;
	   gbcPanel0.gridy = 4;
	   gbcPanel0.gridwidth = 13;
	   gbcPanel0.gridheight = 1;
	   gbcPanel0.fill = GridBagConstraints.BOTH;
	   gbcPanel0.weightx = 1;
	   gbcPanel0.weighty = 1;
	   gbcPanel0.anchor = GridBagConstraints.NORTH;
	   gbPanel0.setConstraints( labelNombreObjeto, gbcPanel0 );
	   pnPanel0.add( labelNombreObjeto );

	   btnCoger = new JButton( "Coger"  );
	   gbcPanel0.gridx = 4;
	   gbcPanel0.gridy = 10;
	   gbcPanel0.gridwidth = 5;
	   gbcPanel0.gridheight = 1;
	   gbcPanel0.fill = GridBagConstraints.BOTH;
	   gbcPanel0.weightx = 1;
	   gbcPanel0.weighty = 0;
	   gbcPanel0.anchor = GridBagConstraints.NORTH;
	   gbPanel0.setConstraints( btnCoger, gbcPanel0 );
	   pnPanel0.add( btnCoger );
	   btnCoger.addActionListener(this);

	   btnInteractuar = new JButton( "Interactuar"  );
	   gbcPanel0.gridx = 12;
	   gbcPanel0.gridy = 10;
	   gbcPanel0.gridwidth = 5;
	   gbcPanel0.gridheight = 1;
	   gbcPanel0.fill = GridBagConstraints.BOTH;
	   gbcPanel0.weightx = 1;
	   gbcPanel0.weighty = 0;
	   gbcPanel0.anchor = GridBagConstraints.NORTH;
	   gbPanel0.setConstraints( btnInteractuar, gbcPanel0 );
	   pnPanel0.add( btnInteractuar );
	   btnInteractuar.addActionListener(this);

	   labelAdvertencia = new JLabel( "Mensaje"  );
	   gbcPanel0.gridx = 4;
	   gbcPanel0.gridy = 1;
	   gbcPanel0.gridwidth = 13;
	   gbcPanel0.gridheight = 1;
	   gbcPanel0.fill = GridBagConstraints.BOTH;
	   gbcPanel0.weightx = 1;
	   gbcPanel0.weighty = 1;
	   gbcPanel0.anchor = GridBagConstraints.NORTH;
	   gbPanel0.setConstraints( labelAdvertencia, gbcPanel0 );
	   pnPanel0.add( labelAdvertencia );

	   labelDescripcion = new JLabel( "descripcion"  );
	   gbcPanel0.gridx = 4;
	   gbcPanel0.gridy = 6;
	   gbcPanel0.gridwidth = 13;
	   gbcPanel0.gridheight = 3;
	   gbcPanel0.fill = GridBagConstraints.BOTH;
	   gbcPanel0.weightx = 1;
	   gbcPanel0.weighty = 3;
	   gbcPanel0.anchor = GridBagConstraints.NORTH;
	   gbPanel0.setConstraints( labelDescripcion, gbcPanel0 );
	   pnPanel0.add( labelDescripcion );

	   setContentPane( pnPanel0 );
	   pack();
	   setVisible( true );
	   setSize(500,300);
	}

	public void setObjetoVisualizar(int idObjeto) {
		this.idObjetoVisualizar = idObjeto;
		String StringDescripcion = Principal.DBObjetos.obtenerObj(idObjeto).getDescripcionObjeto();
		String StringNombre = Principal.DBObjetos.obtenerObj(idObjeto).getNombreObjeto();
		labelDescripcion.setText(StringDescripcion);
		labelNombreObjeto.setText(StringNombre);
		labelAdvertencia.setText("");
	}
	
	public void MostrarAdvertencia(String advertencia) {
		labelAdvertencia.setText(advertencia);
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		// TODO Auto-generated method stub
		int tipoObjeto = Principal.DBObjetos.obtenerObj(idObjetoVisualizar).getTipoObjeto();
		int idObjeto = Principal.DBObjetos.obtenerObj(idObjetoVisualizar).getObjetoID();
		if(ae.getSource() == btnCoger) {
			if(tipoObjeto == 3) {
				MostrarAdvertencia("Los perros no se pueden coger, solo mandar a tomar por saco con un hueso");
			}
			else if(idObjeto == 1 || idObjeto == 2) {
				if(!Principal.J1.TieneObjeto(Principal.DBObjetos.obtenerObj(idObjeto))) {
					Principal.J1.DarObjeto(Principal.DBObjetos.obtenerObj(idObjeto));
					MostrarAdvertencia("Has cogido la llave.");
				} else {
					MostrarAdvertencia("Ya cogiste la llave meloncito");
				}
			}
			else if(idObjeto == 16) {
				Principal.MostrarMensajeAdvertencia("El cuchillo solo se usa para matar a la gallina");
			}
			else if(idObjeto == 0) {
				MostrarAdvertencia("Eso no se coge, puede provocar serios problemas emocionales a los que odian eso");
			}
			else if(idObjeto == 7) {
				if(!Principal.J1.TieneObjeto(Principal.DBObjetos.obtenerObj(idObjeto))) {
					Principal.J1.DarObjeto(Principal.DBObjetos.obtenerObj(idObjeto));
					MostrarAdvertencia("Has cogido el mata ratas");
				} else {
					MostrarAdvertencia("Ya tienes un mata ratas");
				}
			}
			
			else if(idObjeto == 18) {
				if(!Principal.J1.TieneObjeto(Principal.DBObjetos.obtenerObj(18))) {
					Principal.J1.DarObjeto(Principal.DBObjetos.obtenerObj(18));
					MostrarAdvertencia("Has cogido una gallina, busca un cuchillo para matarlo");
				} else {
					MostrarAdvertencia("Ya tienes una gallina, no seas un gallito");
				}
			}
			else if(idObjeto == 4) {
				if(!Principal.J1.TieneObjeto(Principal.DBObjetos.obtenerObj(4))) {
					Principal.J1.DarObjeto(Principal.DBObjetos.obtenerObj(4));
					MostrarAdvertencia("Has cogido la escalera");
				}
			} else if(idObjeto == 3) {
				if(Principal.PerroFuera == true) {
					if(Principal.J1.TieneObjeto(Principal.DBObjetos.obtenerObj(4))) {
						if(!Principal.J1.TieneObjeto(Principal.DBObjetos.obtenerObj(3))) {
							Principal.J1.DarObjeto(Principal.DBObjetos.obtenerObj(3));
							MostrarAdvertencia("Has cogido la llave, ya puedes huir de este infierno");
						}
						else {
							MostrarAdvertencia("Ya tienes la llave.");
						}
					} else {
						MostrarAdvertencia("No tienes una escalera para llegar a la llave");
						
					}
				}else {
					MostrarAdvertencia("El bulldog te mira con cara de rabia");
				}
			}
		} else if(ae.getSource() == btnInteractuar) {
			if(tipoObjeto == 3) {
				if(Principal.J1.TieneObjeto(Principal.DBObjetos.obtenerObj(9))) {
					Principal.J1.RetirarObjeto(Principal.DBObjetos.obtenerObj(9));
					//Principal.J1.DarObjeto(Principal.DBObjetos.obtenerObj(3));
					if(Principal.PerroFuera != true) {
						Principal.PerroFuera = true;
						Principal.MostrarMensajeAdvertencia("El bulldog se ha quedado contento contigo, ya no te quiere matar");
					} else {
						Principal.MostrarMensajeAdvertencia("No lo molestes vaya a ser que cambie de opinion");
					}
					
				} else {
					if(Principal.PerroFuera != true) {
						MostrarAdvertencia("El bulldog es un malcriado y no muere con nada, solo quiere un hueso de gallina");
					}
					else {
						Principal.MostrarMensajeAdvertencia("Ya se quedo contento, largate antes que cambie de opinion");
					}
				}
			} 
			else if(idObjeto == 16) {
				if(Principal.J1.TieneObjeto(Principal.DBObjetos.obtenerObj(18))) {
					Principal.J1.DarObjeto(Principal.DBObjetos.obtenerObj(9));
					Principal.J1.RetirarObjeto(Principal.DBObjetos.obtenerObj(18));
					MostrarAdvertencia("Has cogido un hueso de la gallina kiriki");
				} else {
					MostrarAdvertencia("No tienes una gallina a la que matar");
				}
			}
			
			
			
			
		}
		Principal.v.cambiarObjetosInventario();
	} 
}
